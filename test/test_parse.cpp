#include <catch2/catch.hpp>

#include <calc.hpp>

TEST_CASE("Parse 10+100", "[Parse]") {
  std::string input = "10+100";
  auto output = parse(input);

  REQUIRE(output[0].type == item_type::number);
  REQUIRE(output[0].numeric == 10.0);

  REQUIRE(output[1].type == item_type::operation);
  REQUIRE(output[1].op == operation::plus);

  REQUIRE(output[2].type == item_type::number);
  REQUIRE(output[2].numeric == 100.0);
}

TEST_CASE("Parse (10+100)", "[Parse]") {
  std::string input = "(10+100)";
  auto output = parse(input);

  REQUIRE(output[0].type == item_type::operation);
  REQUIRE(output[0].op == operation::parenthesis_open);

  REQUIRE(output[1].type == item_type::number);
  REQUIRE(output[1].numeric == 10.0);

  REQUIRE(output[2].type == item_type::operation);
  REQUIRE(output[2].op == operation::plus);

  REQUIRE(output[3].type == item_type::number);
  REQUIRE(output[3].numeric == 100.0);

  REQUIRE(output[4].type == item_type::operation);
  REQUIRE(output[4].op == operation::parenthesis_close);
}

TEST_CASE("Parse (10+100)*(5+2000)", "[Parse]") {
  std::string input = "(10+100)*(5+2000)";
  auto output = parse(input);

  REQUIRE(output[0].type == item_type::operation);
  REQUIRE(output[0].op == operation::parenthesis_open);

  REQUIRE(output[1].type == item_type::number);
  REQUIRE(output[1].numeric == 10.0);

  REQUIRE(output[2].type == item_type::operation);
  REQUIRE(output[2].op == operation::plus);

  REQUIRE(output[3].type == item_type::number);
  REQUIRE(output[3].numeric == 100.0);

  REQUIRE(output[4].type == item_type::operation);
  REQUIRE(output[4].op == operation::parenthesis_close);

  REQUIRE(output[5].type == item_type::operation);
  REQUIRE(output[5].op == operation::multiply);

  REQUIRE(output[6].type == item_type::operation);
  REQUIRE(output[6].op == operation::parenthesis_open);

  REQUIRE(output[7].type == item_type::number);
  REQUIRE(output[7].numeric == 5.0);

  REQUIRE(output[8].type == item_type::operation);
  REQUIRE(output[8].op == operation::plus);

  REQUIRE(output[9].type == item_type::number);
  REQUIRE(output[9].numeric == 2000.0);

  REQUIRE(output[10].type == item_type::operation);
  REQUIRE(output[10].op == operation::parenthesis_close);
}