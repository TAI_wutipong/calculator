#include <catch2/catch.hpp>

#include <calc.hpp>

TEST_CASE("Convert 10+100", "[Convert]") {
  std::vector<item> input = {
      item{.type = item_type::number, .numeric = 10.0},
      item{.type = item_type::operation, .op = operation::plus},
      item{.type = item_type::number, .numeric = 100.0}};

  auto output = convert(input);

  REQUIRE(output.front().type == item_type::number);
  REQUIRE(output.front().numeric == 10.0);

  output.pop();

  REQUIRE(output.front().type == item_type::number);
  REQUIRE(output.front().numeric == 100.0);

  output.pop();

  REQUIRE(output.front().type == item_type::operation);
  REQUIRE(output.front().op == operation::plus);
}

TEST_CASE("Convert (10+100)", "[Parse]") {
  std::vector<item> input = {
      item{.type = item_type::operation, .op = operation::parenthesis_open},
      item{.type = item_type::number, .numeric = 10.0},
      item{.type = item_type::operation, .op = operation::plus},
      item{.type = item_type::number, .numeric = 100.0},
      item{.type = item_type::operation, .op = operation::parenthesis_close}};

  auto output = convert(input);

  REQUIRE(output.front().type == item_type::number);
  REQUIRE(output.front().numeric == 10.0);

  output.pop();

  REQUIRE(output.front().type == item_type::number);
  REQUIRE(output.front().numeric == 100.0);

  output.pop();

  REQUIRE(output.front().type == item_type::operation);
  REQUIRE(output.front().op == operation::plus);
}

TEST_CASE("Convert 5*(10+100)", "[Parse]") {
  std::vector<item> input = {
      item{.type = item_type::number, .numeric = 5.0},
      item{.type = item_type::operation, .op = operation::multiply},
      item{.type = item_type::operation, .op = operation::parenthesis_open},
      item{.type = item_type::number, .numeric = 10.0},
      item{.type = item_type::operation, .op = operation::plus},
      item{.type = item_type::number, .numeric = 100.0},
      item{.type = item_type::operation, .op = operation::parenthesis_close}};

  auto output = convert(input);

  REQUIRE(output.front().type == item_type::number);
  REQUIRE(output.front().numeric == 5.0);

  output.pop();

  REQUIRE(output.front().type == item_type::number);
  REQUIRE(output.front().numeric == 10.0);

  output.pop();

  REQUIRE(output.front().type == item_type::number);
  REQUIRE(output.front().numeric == 100.0);

  output.pop();

  REQUIRE(output.front().type == item_type::operation);
  REQUIRE(output.front().op == operation::plus);

  output.pop();

  REQUIRE(output.front().type == item_type::operation);
  REQUIRE(output.front().op == operation::multiply);
}

TEST_CASE("Convert (10+100)-10", "[Parse]") {
  std::vector<item> input = {
      item{.type = item_type::operation, .op = operation::parenthesis_open},
      item{.type = item_type::number, .numeric = 10.0},
      item{.type = item_type::operation, .op = operation::plus},
      item{.type = item_type::number, .numeric = 100.0},
      item{.type = item_type::operation, .op = operation::parenthesis_close},
      item{.type = item_type::operation, .op = operation::minus},
      item{.type = item_type::number, .numeric = 10.0},
  };

  auto output = convert(input);

  REQUIRE(output.front().type == item_type::number);
  REQUIRE(output.front().numeric == 10.0);

  output.pop();

  REQUIRE(output.front().type == item_type::number);
  REQUIRE(output.front().numeric == 100.0);

  output.pop();

  REQUIRE(output.front().type == item_type::operation);
  REQUIRE(output.front().op == operation::plus);

  output.pop();

  REQUIRE(output.front().type == item_type::number);
  REQUIRE(output.front().numeric == 10.0);

  output.pop();

  REQUIRE(output.front().type == item_type::operation);
  REQUIRE(output.front().op == operation::minus);
}