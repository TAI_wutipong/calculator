#include <catch2/catch.hpp>

#include <calc.hpp>

TEST_CASE("Calculate 10+100", "[Calculate]") {
  std::queue<item> input({
      item{.type = item_type::number, .numeric = 10.0},
      item{.type = item_type::number, .numeric = 100.0},
      item{.type = item_type::operation, .op = operation::plus},
  });

  auto output = calculate(input);

  REQUIRE(output == 110.0);
}

TEST_CASE("Calculate 5*(10+100)", "[Calculate]") {

  std::queue<item> input(
      {item{.type = item_type::number, .numeric = 5.0},
       item{.type = item_type::number, .numeric = 10.0},
       item{.type = item_type::number, .numeric = 100.0},
       item{.type = item_type::operation, .op = operation::plus},
       item{.type = item_type::operation, .op = operation::multiply}});

  auto output = calculate(input);

  REQUIRE(output == 550.0);
}