#include "calc.hpp"

#include <stack>

std::vector<item> parse(const std::string &input) {
  std::vector<item> output;

  for (auto current_char : input) {
    if (current_char >= '0' && current_char <= '9') {

      if (output.empty() || output.rbegin()->type != item_type::number) {
        auto new_item = item{.type = item_type::number, .numeric = 0.0};
        output.push_back(new_item);
      }

      auto last_item = output.rbegin();

      last_item->numeric *= 10;

      int digit = (int)current_char - '0';
      last_item->numeric += digit;

      continue;
    }

    auto op_item = item{.type = item_type::operation, .op = operation::none};
    operation &op = op_item.op;

    switch (current_char) {
    case '+':
      op = operation::plus;
      break;

    case '-':
      op = operation::minus;
      break;

    case '*':
      op = operation::multiply;
      break;

    case '/':
      op = operation::divide;
      break;

    case '%':
      op = operation::modulo;
      break;

    case '(':
      op = operation::parenthesis_open;
      break;

    case ')':
      op = operation::parenthesis_close;
      break;
    }

    output.push_back(op_item);
  }

  return output;
}

namespace {
int precedence(const operation &op) {
  switch (op) {
  default:
    return -1;
  case operation::plus:
  case operation::minus:
    return 0;
  case operation::multiply:
  case operation::divide:
  case operation::modulo:
    return 1;
  case operation::parenthesis_open:
  case operation::parenthesis_close:
    return 2;
  }
}
} // namespace

std::queue<item> convert(const std::vector<item> &input) {
  std::stack<item> operatorStack;
  std::queue<item> output;

  for (auto it : input) {
    if (it.type == item_type::number) {
      output.push(it);
      continue;
    }

    if (it.op == operation::parenthesis_open) {
      operatorStack.push(it);
      continue;
    }

    if (it.op == operation::parenthesis_close) {
      while (true) {
        if (operatorStack.empty()) {
          break;
        }

        auto top = operatorStack.top();
        operatorStack.pop();

        if (top.op == operation::parenthesis_open) {
          break;
        }

        output.push(top);
      }
      continue;
    }

    if (!operatorStack.empty()) {
      auto top = operatorStack.top();
      if (top.op != operation::parenthesis_open &&
          precedence(top.op) > precedence(it.op)) {
        operatorStack.pop();
        output.push(top);
      }
    }
    operatorStack.push(it);
  }

  while (!operatorStack.empty()) {
    item op = operatorStack.top();
    operatorStack.pop();

    output.push(op);
  }

  return output;
}

double calculate(const std::queue<item> &items) {
  std::stack<item> numberStack;
  auto all_items = items;

  while (!all_items.empty()) {
    auto it = all_items.front();
    all_items.pop();

    if (it.type == item_type::number) {
      numberStack.push(it);
      continue;
    }

    auto operand2 = numberStack.top();
    numberStack.pop();

    auto operand1 = numberStack.top();
    numberStack.pop();

    item outcome{.type = item_type::number};

    switch (it.op) {
    case operation::plus:
      outcome.numeric = operand1.numeric + operand2.numeric;
      break;

    case operation::minus:
      outcome.numeric = operand1.numeric - operand2.numeric;
      break;

    case operation::multiply:
      outcome.numeric = operand1.numeric * operand2.numeric;
      break;

    case operation::divide:
      outcome.numeric = operand1.numeric / operand2.numeric;
      break;

    case operation::modulo:
      outcome.numeric = fmod(operand1.numeric, operand2.numeric);
      break;
    }

    numberStack.push(outcome);
  }

  return numberStack.top().numeric;
}