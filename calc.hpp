#pragma once

#include <any>
#include <queue>
#include <string>
#include <vector>

enum class operation {
  none,
  plus,
  minus,
  multiply,
  divide,
  modulo,
  parenthesis_open,
  parenthesis_close

};

enum class item_type { invalid, number, operation };

struct item {
  item_type type;
  double numeric;
  operation op;
};

std::vector<item> parse(const std::string &input);
std::queue<item> convert(const std::vector<item> &input);
double calculate(const std::queue<item> &items);