#include <fstream>
#include <iostream>
#include <sstream>

#include <calc.hpp>

using namespace std;

int main(int argc, char **argv) {
  if (argc != 2) {
    cerr << "invalid arguments" << endl;
    return 0;
  }

  std::string line;
  ifstream infile(argv[1]);
  ofstream outfile("answer.txt");

  while (std::getline(infile, line)) {
    auto parsed = parse(line);
    auto converted = convert(parsed);
    auto result = calculate(converted);

    outfile << result << endl;
  }

  outfile.close();
  infile.close();
}